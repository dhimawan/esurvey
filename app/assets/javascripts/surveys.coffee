$(document).ready ->
  $("body").tooltip({ selector: '.tooltips' })

  getTarget = (elem) ->
    return '.' + elem.attr('data-target')+elem.attr('data-index')

  generateList = (target, data, idx) ->
    elemt = "<li class='rangking-item-"+idx+"' data-value='"+data+"'>"
    elemt += "<i class='fa fa-sort icons-move handle'></i>"
    elemt += "<select class='select-ranking-"+idx+"'>"
    elemt += "<option>1</option><option>2</option>"
    if target.hasClass('hasNA')
      elemt += "<option>"+$('.na-'+idx).val()+"</option>"
    elemt += "</select>"
    elemt += "<span class='ranking-question'>"+data+"</span>"
    elemt += "</li>"
    return elemt

  generateListMultiple = (target, data, idx, type) ->
    elemt = "<li class='rangking-item-"+idx+"' data-value='"+data+"'>"
    elemt += "<i class='fa fa-sort icons-move handle'></i>"
    elemt += "<input type='"+type+"' />"
    elemt += "<span class='ranking-question'>"+data+"</span>"
    elemt += "</li>"
    return elemt

  generateMultiple = (target, data, idx, eval2) ->
    elemt = "<tr class='eval-item-"+idx+"' data-value='"+data+"'>"
    elemt += "<td><i class='fa fa-sort icons-move handle'></i><span class='color-black'>"
    elemt += data
    elemt += "</span></td>"
    
    cols = parseInt $('.evals-column-'+idx).val()
    i = 1
    x = (Math.random() * 6)
    while i <= 3
      if i <= cols
        elemt += '<td class="column-eval1-'+idx+' col'+i+'">'
      else
        elemt += '<td class="column-eval1-'+idx+' col'+i+' hide">'
      elemt += '<input type="text" name="dummi'+idx+x+'" class="span12"></td>'
      i++
    elemt += "</tr>"
    return elemt

  generateEval = (target, data, idx, eval2) ->
    elemt = "<tr class='eval-item-"+idx+"' data-value='"+data+"'>"
    elemt += "<td><i class='fa fa-sort icons-move handle'></i><span class='color-black'>"
    elemt += data
    elemt += "</span></td>"
    
    cols = parseInt $('.evals-column-'+idx).val()
    i = 1
    x = (Math.random() * 6)
    while i <= 3
      if i <= cols
        elemt += '<td class="column-eval1-'+idx+' col'+i+'">'
      else
        elemt += '<td class="column-eval1-'+idx+' col'+i+' hide">'
      elemt += '<label class="radio"><input type="radio" name="dummi'+idx+x+'" value="option2"></label></td>'
      i++
    if eval2
      elemt += "<td></td>"
      elemt += '<td><label class="radio"><input type="radio" name="optionsRadios1" value="option2"></label></td>'
      elemt += '<td><label class="radio"><input type="radio" name="optionsRadios1" value="option2"></label></td>'
      elemt += '<td><label class="radio"><input type="radio" name="optionsRadios1" value="option2"></label></td>'
      elemt += '<td><label class="radio"><input type="radio" name="optionsRadios1" value="option2"></label></td>'
    elemt += "</tr>"
    return elemt

  removeNA = (selects) ->
    selects.each ->
      if $(this).children().length > 2
        $(this).children().last().remove()

  # text preview
  $(document).on "change paste keyup", ".text-preview", ->
    $( getTarget $(this) ).html $(this).val()

  #dropdown preview
  $(document).on "change paste keyup", ".dropdown-preview", ->
    target = $( getTarget( $(this) ) )
    # prepend fist child
    if $(this).hasClass('first-value')
      target.children().first().remove()
      target.prepend new Option($(this).val(), '')
    else
      #remove all option except first one
      $( getTarget( $(this) ) + ' option:gt(0)').remove()
      options = $(this).val().split("\n")
      $.each options, (index, value) ->
        target.append new Option(value, value)
        return

  # tooltips
  $(document).on "change", ".is-tooltip", ->
    target = $( getTarget( $(this) ) )
    note = $('.'+$(this).attr('data-value-from') ).val()
    note_block = $('.note-block-'+$(this).attr('data-index'))

    if $(this).is(':checked')
      target.addClass "tooltips"
      target.attr 'data-original-title', note
      $('.tooltips').tooltip()
      note_block.html('')
    else
      note_block.html(note)
      target.removeClass "tooltips"

  # note field
  $(document).on "change paste keyup", ".note-preview", ->
    target = $( getTarget( $(this) ) )
    note = $(this).val()
    note_block = $('.note-block-'+$(this).attr('data-index'))

    if target.hasClass('tooltips')
      target.attr 'data-original-title', note
      $('.tooltips').tooltip()
      note_block.html('')
    else
      note_block.html(note)
      target.removeClass "tooltips"

  # rangking
  $(document).on "change paste keyup", '.dropdown-ranking-preview', ->
    target = $( getTarget( $(this) ) )
    target.children().not('.foot').remove()

    options = $(this).val().split("\n")
    idx = $(this).attr('data-index')
    el = $(this)
    $.each options, (index, value) ->
      if value != ''
        if el.hasClass 'from_multiple'
          type = el.attr('data-type')
          target.append generateListMultiple(target, value, idx, type)
        else
          target.append generateList(target, value, idx)
      return

  # ranking na checkbox
  $(document).on "change", ".is-na", ->
    target = $( getTarget( $(this) ) )
    vals = $('.na-'+$(this).attr('data-index') ).val()
    elem = $(this)
    selects = $('.select-ranking-'+$(this).attr('data-index'))

    if $(this).is(':checked')
      removeNA(selects)
      target.addClass "hasNA"
      selects.each ->
        $(this).append "<option>"+$('.na-'+elem.attr('data-index')).val()+"</option>"
    else
      target.removeClass("hasNA")
      removeNA(selects)

  # rangking na value
  $(document).on "change paste keyup", '.na', ->
    selects = $('.select-ranking-'+$(this).attr('data-index'))
    elem = $(this)
    target = $( getTarget( $(this) ) )
    if target.hasClass('hasNA')
      selects.each ->
        $(this).children().last().html elem.val()

  #ranking reorder
  $("ul.rangking-list").sortable
    handle: ".handle"
    update: (event, ui) ->
      text = ""
      idx = $(this).attr('data-index')
      $(".rangking-item-"+idx).each (i) ->
        text += $(this).attr('data-value')
        text += "\n"
      $(".text-rangking-"+idx).val text.trim()

  # Eval 1
  # hide open box
  $(document).on "change", ".eval1-check", ->
    target = $( getTarget( $(this) ) )
    if $(this).is(':checked')
      target.removeClass("hide")
    else
      target.addClass("hide")

  #openbox label
  $(document).on "change paste keyup", '.eval1-foot-label', ->
    target = $( getTarget( $(this) ) )
    target.html $(this).val()

  #openbox width
  $(document).on "change paste keyup", '.eval1-width', ->
    target = $( getTarget( $(this) ) )
    target.css('width', $(this).val()+'px')

  #column select
  $(document).on "change", '.eval1-column', ->
    target = $( getTarget( $(this) ) )
    sel_val = $(this).val()
    target.removeClass('hide')
    target.each (i) ->
      str = $(this).attr('class')
      indx = str.substring(str.lastIndexOf(' ')).split('col')[1]
      if indx > sel_val
        $(this).addClass('hide')

  #column label
  $(document).on "change paste keyup", '.label-eval1', ->
    str = $(this).attr('class')
    idx = $(this).attr('data-index')
    indx = str.substring(str.lastIndexOf(' '))
    $('.label-eval1-'+idx).filter('.'+indx.substring(1)).html $(this).val()

  #order eval1
  $(".eval-list").sortable
    handle: ".handle"
    update: (event, ui) ->
      text = ""
      idx = $(this).attr('data-index')
      $(".eval-item-"+idx).each (i) ->
        text += $(this).attr('data-value')
        text += "\n"
      $(".text-eval1-"+idx).val text.trim()

  #eval1 prev
  $(document).on "change paste keyup", '.eval1-preview', ->
    target = $( getTarget( $(this) ) )
    target.children().remove()
    el = $(this)

    options = $(this).val().split("\n")
    idx = $(this).attr('data-index')
    $.each options, (index, value) ->
      if value != ''
        if el.hasClass 'eval2'
          target.append generateEval(target, value, idx, true)
        else if el.hasClass 'multiple_answer'
          target.append generateMultiple(target, value, idx)
        else
          target.append generateEval(target, value, idx, false)
      return

  #eval2 weighting
  $(document).on "change paste keyup", ".weighting", ->
    $(".weighting-"+$(this).attr('data-index')).html $(this).val()

  # file upload
  $(document).on "click", ".uploads", ->
    $(this).next().click()
    return false

  $(document).on "click", ".selectAll", ->
    $.each $(this).parent().next().children(), (i, v) ->
      $(v).children().first().addClass "checked"
    return false

  $(document).on "click", ".deselectAll", ->
    $.each $(this).parent().next().children(), (i, v) ->
      $(v).children().first().removeClass "checked"
    return false

  $(document).on "change paste keyup", ".browse_btn", ->
    target = $( getTarget( $(this) ) )
    target.html $(this).val()

  #multiline
  $(document).on "change paste keyup", '.multilinewidth', ->
    target = $( getTarget( $(this) ) )
    target.css('width', $(this).val()+'px')

  $(document).on "change paste keyup", '.multilinevalue', ->
    target = $( getTarget( $(this) ) )
    target.val($(this).val())
  
  #precentage
  $(document).on "change paste keyup", '.afterbefore', ->
    target = $( getTarget( $(this) ) )
    target.html($(this).val())

  $(document).on "change paste keyup", '.pdefault', ->
    target = $( getTarget( $(this) ) )
    target.val($(this).val())

   $(document).on "change paste keyup", '.pstep', ->
    target = $( getTarget( $(this) ) )
    target.attr('step',$(this).val())

  # popup add page
  $('#add_page').click ->
    $('#pageForm').modal('show')

  # popup add page
  $('.customize_final_page').click ->
    $('#customize_final_page_form').modal('show')

  # popup add page
  $('.display_final_page').click ->
    $('#display_final_page_form').modal('show')

  #validate new page
  $('#new_page').validate()

  $(".hide").hide()

  # popup move Question
  $('.move_question').click ->
    $('#moveQuestionForm').modal('show')

  # layout coloring font color
  $('#style_font_color').colorpicker().on 'changeColor.colorpicker', (event) ->
    $(".font-color").css({"color": event.color.toHex()})
    return

  # layout coloring Background Color
  $('#style_background_color').colorpicker().on 'changeColor.colorpicker', (event) ->
    $(".question_list-prev").css({"background-color": event.color.toHex()})
    return
  
    
  # layout coloring row color
  $('#style_row_color').colorpicker().on 'changeColor.colorpicker', (event) ->
    $(".question_choise-prev").css({"background-color": event.color.toHex()})
    return

    
  # layout coloring button font color
  $('#style_button_font_color').colorpicker().on 'changeColor.colorpicker', (event) ->
    $(".btn-font-color").css({"color": event.color.toHex()})
    return

  # layout coloring button background color
  $('#style_button_background_color').colorpicker().on 'changeColor.colorpicker', (event) ->
    $(".btn-font-color").css({"background-color": event.color.toHex()})
    return  

  # layout coloring button border color
  $('#style_button_border_color').colorpicker().on 'changeColor.colorpicker', (event) ->
    $(".btn-font-color").css({"border-color": event.color.toHex()})
    return  

  # layout coloring Top Border color
  $('#style_top_border_color').colorpicker().on 'changeColor.colorpicker', (event) ->
    $(".question_list-prev").css({"border-top-color": event.color.toHex()})

    return

  # layout coloring bottom Border color
  $('#style_bottom_border_color').colorpicker().on 'changeColor.colorpicker', (event) ->
    $(".question_list-prev").css({"border-bottom-color": event.color.toHex()})

    return

  # layout coloring Left Border color
  $('#style_left_border_color').colorpicker().on 'changeColor.colorpicker', (event) ->
    $(".question_list-prev").css({"border-left-color": event.color.toHex()})

    return

  # layout coloring Right Border color
  $('#style_right_border_color').colorpicker().on 'changeColor.colorpicker', (event) ->
    $(".question_list-prev").css({"border-right-color": event.color.toHex()})
    return

  # layout coloring width box 
  $('#style_width_box').keyup ->
    $(".question_list-prev").css 'width': $(this).val()
    return

  # layout coloring height box 
  $('#style_height_box').keyup ->
    $('.question_list-prev').css 'height': $(this).val()
    return

