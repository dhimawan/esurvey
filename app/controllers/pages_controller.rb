class PagesController < ApplicationController
  respond_to :html, :json, :js

  def new
  end

  def edit
    @page = Page.find(params[:id])
    respond_to do |format|
      format.js
    end
  end

  def update
    if params[:move].present?
      page = Page.find(params[:move][:page_id])
      page.update_attributes({:survey_id => params[:move][:survey_id]})
    else
      unless params[:page][:id].blank?
        page = Page.find(params[:page][:id])
        page.update_attributes({:page_title => params[:page][:page_title],:page_description => params[:page][:page_description]})   
      end
    end
    respond_to do |format|
      format.js
    end
  end

  def create
    params[:page][:survey_id] = params[:survey_id]
    @page = Page.new(secure_params)
    respond_to do |format|
      if @page.save
        format.html {redirect_to survey_url(@page.survey_id)}
        format.js
      end
    end
  end

  def destroy
    unless params[:id].blank?
      page = Page.find(params[:id])
      page.destroy
    end
    respond_to do |format|
      format.js
    end
  end

  def copy
    status, page = Page.copy_page_question(params)
    respond_to do |format|
      format.js
    end
  end

  def move
    @surveys = Survey.where("id != ? ", params[:survey_id])
    @page_id = params[:id]
    respond_to do |format|
      format.js
    end

  end

  def page_questions
    @type = params[:id]
    @page_id = params[:page_id]
    time = Time.new
    @index = time.to_i
    @question = {}
    respond_to do |format|
      format.js
    end
  end

  private
  def secure_params
    params.require(:page).permit(:page_title, :survey_id, :page_description,:page_url)
  end

end
