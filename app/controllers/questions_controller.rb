class QuestionsController < ApplicationController

  def create
    @status,@message = Question.save_with_detail(params)
    @page_id = params[:page_id]
    @page = Page.find(@page_id)
    respond_to do |format|
      format.js
    end
  end

  def destroy
    question = Question.find(params[:id])
    @page_id = params[:page_id]
    @page = Page.find(@page_id)

    respond_to do |format|
      format.js if question.destroy
    end
  end

  def copy
    question = Question.find(params[:id])
    @page_id = params[:page_id]
    @page = Page.find(@page_id)
    new_question = question.dup
    respond_to do |format|
      format.js if new_question.save!
    end
  end

  def edit
    @question = Question.find(params[:id])
    @page_id = params[:page_id]
    @page = Page.find(@page_id)
    time = Time.new
    @index = time.to_i
    respond_to do |format|
      format.js
    end
  end

  def update
    if params[:move].present?
      @status,@message, @page_id , @prev_page_id = Question.move_question(params)
      @prev_page = Page.find(@prev_page_id)
      @view_type = "move"
    else
      @status,@message, @page_id , @question = Question.update_with_detail(params)
      @view_type = "update"
    end
    @page = Page.find(@page_id)
    respond_to do |format|
      format.js
    end
  end

  def move
    @pages = Page.where("survey_id = ? and id != ? ", params[:survey_id],params[:page_id])
    @question_id = params[:id]
    respond_to do |format|
      format.js
    end
  end

end
