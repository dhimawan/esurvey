class ReportsController < ApplicationController
  before_filter :authenticate_user!

  def index
    @surveys = Survey.page(params[:page])
  end

  def download
    @surveys = Survey.find(params[:id])    
    @survey_answers,@total_user , @users = SurveyAnswer.generate_report(params)
    respond_to do |format|
      format.html
      if params[:type] == "pdf"
        format.pdf do
          @example_text = "some text"
          render :pdf => "file_name",
                 :template => 'reports/download.pdf.erb',
                 :layout => '/layouts/print_layout.html.erb',
                 :footer => {
                    # :center => "Center",
                    # :right => "Right"
                    :right =>'[page] of [topage]' 
                 }
        end
      end
      if params[:type] == "csv"
        format.csv { render text: SurveyAnswer.to_csv(@survey_answers,@total_user,@users) }
      end
    end

  end

end
