class SurveysController < ApplicationController
  before_filter :authenticate_user!
  skip_before_filter :verify_authenticity_token, :only => :next_question_logical_workflow

  def fill
    @survey = Survey.find(params[:survey_id])
  end

  def post_answer
    survey = Survey.find(params[:survey_id])
    status = SurveyAnswer.save_with_detail(params,current_user.id)
    final_page = survey.url_final_page.blank? ? (survey.final_page.blank? ? "/" : survey_final_page_path(params[:survey_id]) ) : survey.url_final_page
    redirect_to final_page , :notice => "Surveys success save"
  end

  def final_page
    @survey = Survey.find(params[:survey_id])
    render layout: false
  end

  def index
    @surveys = Survey.page(params[:page])
  end

  def testing
    render "surveys/new-evan"
  end

  def new
    @survey = Survey.new()
  end

  def create
    o = [('a'..'z'), ('A'..'Z')].map { |i| i.to_a }.flatten
    survey_url = (0...50).map { o[rand(o.length)] }.join
    params[:survey][:survey_url] = survey_url
    
    survey = Survey.new(secure_params)
    if survey.save
      redirect_to :action => "index" , :notice => "save survey success created"
    else
      render :action => "new" , :alert => "save survey errors"
    end
  end

  def customize_final_page
    @survey = Survey.find(params[:survey_id])
    @survey.update_attributes(params.require(:survey).permit(:final_page, :url_final_page))
    respond_to do |format|
      format.js
    end
  end

  def show
    @survey = Survey.find(params[:id])
    @question = Question.new
    @page = Page.new()
  end

  def edit

  end

  def logical_workflow
    @survey = Survey.find(params[:id])
    @logical_dropdown,@logical_array = [] , []
    @survey.pages.each do |page|
      page.questions.each do |pq|
        if pq.question_type == "yes_no" || pq.question_type == "dropdown" || pq.question_type == "radio_box"
          @logical_dropdown << pq
          @logical_array << [pq.question , pq.id]
        end
      end
    end
  end

  def save_logical_workflow
    status = LogicalWorkflow.save_logical_workflow(params)
    if status
      redirect_to surveys_path, :notice => "Logical Workflow success created"
    else
      render logical_workflow , :alert => "Unable to update user."
    end
  end

  def layout_coloring
    @survey = Survey.find(params[:id])
    @layout_coloring = @survey.layout_colorings.first
  end

  def save_layout_coloring
    status,message = LayoutColoring.save_layout_coloring(params,params_coloring)
    if status
      redirect_to surveys_path, :notice => message
    else
      render layout_coloring , :alert => message
    end
  end

  def next_question_logical_workflow
    condition = {:question_id => params[:question_id],:choise_value => params[:choise_answer].first[1]}
    condition = {:question_id => params[:question_id],:choise_index => params[:choise_answer].first[1]} if params[:question_type] == "yes_no"

    lg = LogicalWorkflow.where(condition)
    render :json => lg.first
  end

  private
  def secure_params
    params.require(:survey).permit(:title, :initial_name, :language, :description,:survey_url)
  end

  def params_coloring
    params.require(:style).permit(:font_color,:background_color,:row_color,:button_font_color,:button_background_color,:button_border_color,:top_border_color,:bottom_border_color,:right_border_color,:left_border_color,:width_box,:height_box)
  end

end
