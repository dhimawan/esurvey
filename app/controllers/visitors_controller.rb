class VisitorsController < ApplicationController
  before_filter :authenticate_user!

  def index
    @surveys = Survey.page(params[:page])
  end

end
