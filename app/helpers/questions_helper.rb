module QuestionsHelper

  def generate_choise(type,data, has_na=false, na_value='',processed_view)
    choise_name = processed_view.blank? ? "" : "choise_answer[#{data.id}][]"
    if type =="dropdown"
      
      option = data.choise.split(/\n/)
      style = "background-color:#{data.page.survey.layout_colorings.first.row_color}" unless processed_view.blank?
      select_tag "#{choise_name}", options_for_select(option) ,:class => "choise_answer_#{data.id}",:style => style

    elsif type =="dropdown_rangking"
      
      value_rating = []
      style = "background-color:#{data.page.survey.layout_colorings.first.row_color}" unless processed_view.blank?
      options = data.choise.split(/\n/)
      options.each_with_index{|op,idx| value_rating << idx+1 }
      rangking_opt = has_na ? value_rating+[na_value] : value_rating
      opt = '<ul class="dropdown-ranking-select- rangking-list unstyled" style="margin-left: 0;"  >'
      options.each do |s|
        opt += "<li style=#{style}> <i class='fa fa-sort icons-move handle'></i> "
        opt += select_tag "#{choise_name}", options_for_select(rangking_opt),:class => "choise_answer_#{data.id}"
        opt += "<span class='ranking-question'>#{s}</span> </li>"
      end
      opt +='</ul>'
      raw(opt)
    elsif type=="multiline_text_box"
        text_area_tag("#{choise_name}", data.default_value, style: "#{data.inline_style}")
    elsif type == "evaluation1" || type == "evaluation2" || type == "multiple_answer"
      row = data.choise.split(/\n/)
      column = data.column_label.split(/\n/)
      generateTabel(row,column,type, data)
    elsif type == "multiple_choice" || type == "radio_box"
      row = data.choise.split(/\n/)
      opt = '<ul class="dropdown-ranking-select- rangking-list unstyled" style="margin-left: 0;" >'
      row.each_with_index do |s,index|
        opt += "<li> <i class='fa fa-sort icons-move handle'></i>"
        opt += check_box("choise_answer[#{data.id}]","#{index}",{},s) if type == "multiple_choice"
        opt += radio_button "choise_answer" ,"#{data.id}",s,:class => "choise_answer_#{data.id}"  if type == "radio_box"
        opt += " <span class='ranking-question'>#{s}</span> </li>"
      end
      opt += "<li>"
      opt += check_box("choise_answer[#{data.id}]" ,"#{row.count}",{},"input") if type == "multiple_choice"
      opt += radio_button("choise_answer[#{data.id}]" ,"#{row.count}","input") if type == "radio_box"
      opt += "#{data.column_label} <input type='text' value=''  name='input_answer[#{type}][#{data.id}]'> </li>" 
      opt +='</ul>'
      raw(opt)
    elsif type == "yes_no"
      opt = '<ul class="dropdown-ranking-select- rangking-list unstyled" style="margin-left: 0;" >'
      opt += "<li>"
      opt += radio_button("choise_answer" ,data.id,'0' ,:class => "choise_answer_#{data.id}") 
      opt += "<span class='ranking-question'>Yes </span> </li>"
      opt += "<li>"
      opt += radio_button("choise_answer" ,data.id,'1' ,:class => "choise_answer_#{data.id}")
      opt += "<span class='ranking-question'>No </span> </li>"
      opt += "</ul>"
      raw(opt)
    elsif type == 'percentage'
      opt = '<h4 class="color-black dropdown-question-" data-placement="top" data-toggle="tooltip" href="#" data-original-title=""></h4>'
      opt += "<div>"
      opt += "<span class='pbefore-1 color-black'>#{data.text_before_field}</span>"
      opt += '<input type="range" min="0" max="100" value="1" class="percentage-1">'
      opt += "<span class='pafter-1 color-black'>#{data.text_after_field}</span>"
      opt += "</div>"
      raw(opt)

    elsif type == 'single_question'
      opt = "<input type='text' name='#{choise_name}' style='#{data.inline_style}' value='#{data.default_value}' />"
      raw(opt)
    end
  end

  def generate_choise_for_answer_survey(type,data)
    if type =="dropdown" 
      option = data.split(/\n/)
      select_tag 'choise', options_for_select(option)
    elsif type =="dropdown_rangking"
      rangking_opt = [1,2]
      options = data.split(/\n/)
      opt = '<ul class="dropdown-ranking-select- rangking-list unstyled" style="margin-left: 0;" >'
      options.each do |s|
        opt += "<li> <i class='fa fa-sort icons-move handle'></i>  #{select_tag '', options_for_select(rangking_opt)} <span class='ranking-question'>#{s}</span> </li>"
      end
      opt +='</ul>'
      raw(opt)
    elsif type=="multiline_text_box"
        text_area_tag("multiline_text_box", data.default_value, style: "#{data.inline_style}")
    elsif type == "evaluation1" || type == "evaluation2" || type == "multiple_answer"
      row = data.choise.split(/\n/)
      column = data.column_label.split(/\n/)
      generateTabel(row,column,type)

    elsif type == "multiple_choice" || type == "radio_box"
      row = data.choise.split(/\n/)
      opt = '<ul class="dropdown-ranking-select- rangking-list unstyled" style="margin-left: 0;" >'
      row.each do |s|
        opt += "<li> <i class='fa fa-sort icons-move handle'></i>"
        opt += check_box('','multiple_choice') if type == "multiple_choice"
        opt += radio_button("question[#{data.id}]",'radio_box', s) if type == "radio_box"

        opt += " <span class='ranking-question'>#{s}</span> </li>"
      end
      opt += "<li>"
      opt += check_box('','multiple_choice') if type == "multiple_choice"
      opt += radio_button("question[#{data.id}]" ,'radio_box','') if type == "radio_box"

      opt += "#{data.column_label} <input type='text' value=''  name=''> </li>"
      opt +='</ul>'
      raw(opt)
    elsif type == "yes_no"
      opt = '<ul class="dropdown-ranking-select- rangking-list unstyled" style="margin-left: 0;" >'
      opt += "<li>"
      opt += radio_button("question[#{data.id}]", 'radio_box', 'Yes') 
      opt += "<span class='ranking-question'>#{t(:yes_label)} </span> </li>"
      opt += "<li>"
      opt += radio_button("question[#{data.id}]", 'radio_box', 'No')
      opt += "<span class='ranking-question'>#{t(:no_label)} </span> </li>"
      opt += "</ul>"
      raw(opt)
    end
  end

  def generateTabel(row,column,type,data=0)
    render :partial => "questions/generateTabel",:locals => {:row => row , :col => column ,:type => type, data: data}
  end

  def setValueQuestion(data,name,type)
    value = ""
    unless data.blank?
      value = data.name if name == "name"
      value = data.tooltips if name == "tooltips"
      if name == "first_choise"
        choise = data.choise.split(/\n/)
        value = choise[0]
      end
      if name == "choise"
        value = setValueQuestionChoise(data,type)
      end
    end
    return value
  end

  def setValueQuestionChoise(data,type)
    value = valueChoiseWithoutFirstElement(data) if type == "dropdown"
    value = data.choise if type == "evaluation1" || type == "dropdown_rangking" || type == "evaluation2" || type == 'multiline_text_box' || type == 'multiple_answer'

    return value
  end

  def valueChoiseWithoutFirstElement(data)
    value = ""
    data.choise.split(/\n/).each_with_index do |d,idx|
      unless idx == 0
        value +="#{d}\n " unless d.blank?
      end
    end
    return value
  end

  def generateLogicalView(data,logical_array)
    list_answers = {}
    new_logical_array = []
    value_options = {}
    unless data.question_type =="yes_no"
      list_answers = data.choise.split(/\n/) 
      list_answers.delete_at(0) if data.question_type=="dropdown"
    end
    list_answers = ["yes","no"] if data.question_type =="yes_no"
    logical_array.each{|d| new_logical_array << [truncate(d[0], length: 40),d[1]] unless d.include?(data.id) }
    new_logical_array << ["Final Page",99999]
    list_answers.each_with_index do |la,index|
      value = LogicalWorkflow.where(:question_id => data.id , :choise_index => index)

      value_options["#{index}"] =  (value.blank? ? " " : value.first.choise_question_id)
    end
    render :partial => "surveys/logical_view" ,:locals => {:list_answers => list_answers ,:logical_array => new_logical_array,:data => data,:value_options => value_options }

  end

  def layout_coloring(survey,data,proccessed_view)
    style = []
    layout = survey.layout_colorings.first
    unless layout.blank?  
      unless proccessed_view.blank?
        data.each do |d|
          style << "color:#{layout.font_color}" if d == "font_color"
          style << "background-color:#{layout.background_color}" if d == "background_color" 
          style << "color:#{layout.button_font_color}" if d == "button_font_color"
          style << "background-color:#{layout.button_background_color}" if d == "button_background_color" 
          style << "border-color:#{layout.button_border_color}" if d == "button_border_color" 

          style << 'border-style: solid' if d == "top_border_color"
          style << 'border-width: 4px' if d == "top_border_color"
          style << "border-top-color:#{layout.top_border_color}" if d == "top_border_color"

          style << "border-bottom-color:#{layout.bottom_border_color}" if d == "bottom_border_color" 
          style << "border-right-color:#{layout.right_border_color}" if d == "right_border_color" 
          style << "border-left-color:#{layout.left_border_color}" if d == "left_border_color" 
          style << "width:#{layout.width_box}px" if d == "width_box" 
          style << "height:#{layout.height_box}px" if d == "height_box" 
          

        end 
      end
    end
    join_style = style.join(";")
    layout_style = "style=\'#{join_style}\'"

    raw layout_style
  end

  def generateLogicalDropdown(survey)

  end

end
