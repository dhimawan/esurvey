class LayoutColoring < ActiveRecord::Base
  belongs_to :survey

  def self.save_layout_coloring(params,params_coloring)
    status,message = false , "failed"
    layout_coloring = self.where(:survey_id => params[:id])
    if layout_coloring.blank?
      layoutC = self.new(params_coloring)
      layoutC.survey_id = params[:id]
      layoutC.save
      status,message = true , "Layout Coloring success Created!"
    else
      layout_coloring.first.update_attributes(params_coloring)
      status,message = true , "Layout Coloring success updated!"
    end
    return status , message
  end

end
