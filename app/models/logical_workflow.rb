class LogicalWorkflow < ActiveRecord::Base
  belongs_to :question

  def self.save_logical_workflow(params)
    status , survey = false , Survey.find(params[:id])
    if check_final_page(params)
      params[:logical_workflow][:choise].each do |d|
        d[1].each_with_index do |val,index|
          lwf = self.where(:question_id => d[0],:choise_index => index)
          if lwf.blank?
            new_lwf = self.new({:question_id => d[0],:choise_index => index , :choise_question_id => val[1],:choise_value =>params[:logical_workflow][:choise_value]["#{d[0]}"]["#{index}"] })
            new_lwf.save
          else
            lwf.first.update_attributes({:choise_question_id => val[1]})
          end
        end
      end
      survey.update_attributes({:is_logical_workflow => true})
      status = true
    end
    return status
  end

  def self.check_final_page(params)
    status_valid = true
    params[:logical_workflow][:choise].each do |d|
      d[1].each do |val|
        status_valid = false if val == "99999"
      end
    end
    return status_valid
  end
end
