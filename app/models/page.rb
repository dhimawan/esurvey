class Page < ActiveRecord::Base
  belongs_to :survey
  has_many :questions , dependent: :destroy
  validates :page_title, presence: true

  def self.copy_page_question(params)
    status = false
    page = Page.find(params[:id])
    new_page = page.dup
    if new_page.save
      page.questions.each do |d|
        new_ques = d.dup
        new_ques.page_id = new_page.id
        new_ques.save!
      end
      status,new_page = true ,new_page
    end
    return status , new_page
  end

end
