class Question < ActiveRecord::Base
  belongs_to :page
  has_many :logical_workflows

  default_scope { order('id asc') }

  def self.save_with_detail(params)
    status = true
    if params[:question][:name].count > 0
      data = []
      params[:question][:name].each_with_index do |ques,index|
        data << self.generate_dropdown_array(params,index) if params[:question][:question_type][index]=="dropdown" 

        data << self.generate_dropdown_rangking_array(params,index) if params[:question][:question_type][index]=="dropdown_rangking"

        data << self.generateMultilineTextBox(params,index) if params[:question][:question_type][index]=="multiline_text_box" || params[:question][:question_type][index]=="single_question" 

        data << self.generateEvaluation(params,index) if params[:question][:question_type][index] == "evaluation1" || params[:question][:question_type][index] == "evaluation2"  || params[:question][:question_type][index] == "multiple_answer"

        data << self.generateMultilineChoice(params,index) if params[:question][:question_type][index] == "multiple_choice" || params[:question][:question_type][index] == "radio_box"

        data << self.generateYesNo(params,index) if params[:question][:question_type][index]=="yes_no"

        data << self.generate_percentage(params,index) if params[:question][:question_type][index]=="percentage"

        total_question = Question.where(:page_id => params[:page_id]).count
        # data[:order] = total_question + 1
      end
      ActiveRecord::Base.transaction do
        question = Question.create(data)
        message = "question success create!"
      end
    else
      status = false
      message = "invalid Argument"
    end
    return status , message
  end

  def self.generate_dropdown_array(params,index)
    if params[:question][:question_type][index] == "dropdown"
      choise = params[:question][:first_choise][index]+"\n"+params[:question][:choise][index]
    elsif params[:question][:question_type][index] == "dropdown_rangking"
      choise = params[:question][:choise][index]
    end
    data_hash = {
          :page_id        => params[:page_id],
          :name           => params[:question][:name][index] ,
          :question_type  => params[:question][:question_type][index],
          :question       => params[:question][:name][index],
          :tooltips       => params[:question][:tooltips][index],
          :choise         => choise,
          :as_tooltips    => params[:question][:check_tooltip].nil? ? false : true
    }
    return data_hash
  end

  def self.generate_dropdown_rangking_array(params,index)
    choise = params[:question][:choise][index]
    data_hash = {
          :page_id        => params[:page_id],
          :name           => params[:question][:name][index] ,
          :question_type  => params[:question][:question_type][index],
          :question       => params[:question][:name][index],
          :tooltips       => params[:question][:tooltips][index],
          :choise         => choise,
          :as_tooltips    => params[:question][:check_tooltip].nil? ? false : true,
          :has_not_available => params[:has_not_available].nil? ? false : true,
          :not_available_value => params[:not_available_value]
    }
    return data_hash
  end

  def self.generateMultilineTextBox(params,index)
    width = params[:question][:inline_style][index].nil? ? "" : params[:question][:inline_style][index] 
    data_hash = {
          :page_id        => params[:page_id],
          :name           => params[:question][:name][index] ,
          :question_type  => params[:question][:question_type][index],
          :question       => params[:question][:name][index],
          :tooltips       => params[:question][:tooltips][index],
          :default_value  => params[:question][:default_value][index],
          :inline_style   =>  "width: #{width}px;",
          :as_tooltips    => params[:question][:check_tooltip].nil? ? false : true
    }
    return data_hash
  end

  def self.generateEvaluation(params,index)
    index_val = params[:question_index][index]
    column_label = "" 
    params[:question][:column_label][index_val].each{|d| column_label +="#{d} \n " }
    data_hash = {
          :page_id        => params[:page_id],
          :name           => params[:question][:name][index] ,
          :question_type  => params[:question][:question_type][index],
          :question       => params[:question][:name][index],
          :tooltips       => params[:question][:tooltips][index],
          :choise         => params[:question][:choise][index],
          :column_label   => column_label,
          :description_block => (params[:question][:question_type][index] == "evaluation2" ? params[:question][:description_block][index] : "" ),
          :as_tooltips    => params[:question][:check_tooltip].nil? ? false : true,
          :inline_style   =>  "width: #{params['inline_style'].to_i}px;",
          :text_before_field => params[:text_before_field],
          :open_box_field => params[:append_open_box].nil? ? false : true,
          :column_count   => params[:column_count]
    }
    return data_hash
  end

  def self.generateMultilineChoice(params,index)
    index_val = params[:question_index][index]
    
    width = params[:question][:inline_style_width][index].nil? ? "" : "width: #{params[:question][:inline_style_width][index]}px;"
    if params[:question][:question_type][index] == "multiple_choice"
      max_value = params[:question][:inline_css_max_value][index].nil? ? "" : params[:question][:inline_css_max_value][index] 
    end
    data_hash = {
          :page_id        => params[:page_id],
          :name           => params[:question][:name][index] ,
          :question_type  => params[:question][:question_type][index],
          :question       => params[:question][:name][index],
          :tooltips       => params[:question][:tooltips][index],
          :choise         => params[:question][:choise][index],
          :column_label   => params[:question][:column_label][index],
          :inline_style   => width,
          :as_tooltips    => params[:question][:check_tooltip].nil? ? false : true
    }
    return data_hash
  end

  def self.generate_percentage(params, index)
    data_hash = {page_id: params[:page_id], name: params[:question][:name][index], question_type: params[:question][:question_type][index], question: params[:question][:name][index],
     tooltips: params[:question][:tooltips][index], text_before_field: params[:question][:text_before_field][index], increment_value: params[:question][:increment_value][index],
     text_after_field: params[:question][:text_after_field][index]
    }
    return data_hash
  end

  def self.generateYesNo(params,index)
    data_hash = {
          :page_id        => params[:page_id],
          :name           => params[:question][:name][index] ,
          :question_type  => params[:question][:question_type][index],
          :question       => params[:question][:name][index],
          :tooltips       => params[:question][:tooltips][index],
          :as_tooltips    => params[:question][:check_tooltip].nil? ? false : true
    }
    return data_hash
  end

  def self.update_with_detail(params)
    status = true
    question = Question.find(params[:edit_question_id])
    params[:page_id] = question.page_id
    if params[:question][:name].count > 0
      data = []
      params[:question][:name].each_with_index do |ques,index|
        data << self.generate_dropdown_array(params,index) if params[:question][:question_type][index]=="dropdown" 

        data << self.generate_dropdown_rangking_array(params,index) if params[:question][:question_type][index]=="dropdown_rangking"

        data << self.generateMultilineTextBox(params,index) if params[:question][:question_type][index]=="multiline_text_box" 

        data << self.generateEvaluation(params,index) if params[:question][:question_type][index] == "evaluation1" || params[:question][:question_type][index] == "evaluation2"  || params[:question][:question_type][index] == "multiple_answer" 

        data << self.generateMultilineChoice(params,index) if params[:question][:question_type][index] == "multiple_choice" || params[:question][:question_type][index] == "radio_box"

        data << self.generateYesNo(params,index) if params[:question][:question_type][index]=="yes_no" 

        # byebug
        
      end
      ActiveRecord::Base.transaction do
        question.update_attributes(data[0])
        message = "question success update!"
      end
    else
      status = false
      message = "invalid Argument"
    end
    return status , message , question.page_id , question
  end

  def self.move_question(params)
    status , message = false , "invalid Argument"
    question = Question.find(params[:move][:question_id])
    prev_page_id = question.page_id
    unless question.blank?
      question.update_attributes({:page_id => params[:move][:page_id]}) 
      status,message = true , "Question success relocation"
    end
    return status , message,params[:move][:page_id],prev_page_id
  end

end
