class Survey < ActiveRecord::Base
	has_many :pages
	has_many :layout_colorings
	has_many :survey_answers
end
