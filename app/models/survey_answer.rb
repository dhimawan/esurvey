class SurveyAnswer < ActiveRecord::Base
  belongs_to :survey
  belongs_to :user

  def self.save_with_detail(params,user_id)
    status = false

    params[:choise_answer].each do |choise_answer|
      question = Question.find(choise_answer[0])
      data = generate_params(choise_answer,question,params,user_id)
      qa = self.where(:question_id =>choise_answer[0] , :user_id =>user_id)
      if qa.blank?
        survey_answer = self.new(data)
        status = true if survey_answer.save
      else
        status = true if qa.first.update_attributes(data)
      end
    end
    return status
  end

  def self.generate_params(choise_answer,question,params,user_id)
    input = {:question_id => question.id,:user_id => user_id,:survey_id => params[:survey_id]}
    input[:answer] = choise_answer[1].first if question.question_type == "dropdown"
    input[:answer] = choise_answer[1].join("\n") if question.question_type == "dropdown_rangking"
    input[:answer] = choise_answer[0] if question.question_type == "multiline_text_box"
    input[:answer] = choise_answer[1].values.join("\n") if question.question_type == "multiple_choice"
    input[:answer] = choise_answer[1].first if question.question_type == "radio_box"
    input[:answer] = choise_answer[1].first if question.question_type == "single_question"
    input[:answer] = choise_answer[1] if question.question_type == "yes_no"


    if question.question_type == "evaluation1"
      answer = []
      choise_answer[1].each{|d| answer << d[1].keys.first }
      input[:answer] = answer.join("\n")
    end
    
    if question.question_type == "evaluation2"
      answer = []
      choise_answer[1].each do |d| 
        answer << d[1].keys.first+"\t"+ d[1]["#{d[1].keys.last}"]
      end
      input[:answer] = answer.join("\n")
    end

    if question.question_type == "multiple_answer"
      answer = []
      choise_answer[1].each_with_index do |ch , idx|
        answer << ch[1].values.join("\t")
      end
      input[:answer]=answer.join("\n")
    end

    return input
  end

  def self.generate_report(params)
    values = []
    list_users = []
    survey = Survey.find(params[:id])
    survey.pages.each_with_index do |page,index|
      page.questions.each_with_index do |question,qindex|
        values << {
          :question => question.question,
          :user_answer => self.find_question_answer(survey,question.id),
        }
      end
    end
    total_user = survey.survey_answers.group("user_id").count.values.count
    users = survey.survey_answers.group("user_id").count.keys
    users.each{|us| list_users << User.find(us) }

    return values , total_user , list_users
  end

  def self.find_question_answer(survey,question_id)
    user_answer = {}
    sa = survey.survey_answers.where("question_id" => question_id)
    sa.each do |answer|
      user_answer["#{answer.user_id}"] = answer
    end
    return user_answer
  end

  def self.to_csv(survey_answers,total_user, list_users)
    header_name = ['no','question']
              
    list_users.each{|user| header_name << user.name }
    CSV.generate do |csv|
      csv << header_name
      no = 0 

      survey_answers.each_with_index do |qa,index|
        no +=1
        answer = ["#{no}",qa[:question]]
        list_users.each do |us|
          if qa[:user_answer]["#{us.id}"].blank?
            answer << "-"
          else
            answer << qa[:user_answer]["#{us.id}"].answer
          end
        end
        csv << answer
      end
    end

  end

end
