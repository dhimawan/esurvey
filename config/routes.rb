Rails.application.routes.draw do
  # devise_for :users
  
  devise_for :users, :controllers => { :registrations => "users" }
  devise_scope :user do
    # get 'login', to: "devise/sessions#new", as: "login"
    # get 'logout', to: "devise/sessions#destroy", as: "logout"
    # get 'logout', to: "users/sessions#destroy", as: "logout"
    get '/users/sign_out' => 'devise/sessions#destroy' 
    get 'signup', to: "users#new", as: "signup"
    # match '/users/:id', :to => 'users#show', :as => :user
  end

  resources :users
  # resources :surveys
  get "/testing", to: "surveys#testing"
  resources :surveys do
    resources :pages
    put :customize_final_page
    get :final_page
    get :fill
    post :post_answer
  end
  get '/page_question/:id/:page_id' ,to:  "pages#page_questions" ,:as => "page_questions"
  post 'save_page_question/:survey_id/:page_id' , to: "questions#create" , :as => "create_page_question"
  get 'questions/:id/destroy/:page_id' , to: "questions#destroy" , :as => "delete_question_page"
  get 'questions/:id/copy/:page_id' , to: "questions#copy" , :as => "copy_question_page"
  get 'questions/:id/edit/:page_id' , to: "questions#edit" , :as => "edit_question_page"
  put 'questions/update' , to: "questions#update" , :as => "update_question_page"
  get 'questions/:id/move/:page_id/:survey_id' , to: "questions#move" , :as => "move_question_page"
  put 'page/update' , to: "pages#update" , :as => "update_page"
  get 'page/:id/copy' , to: "pages#copy" , :as => "copy_page"
  get 'page/:id/move/:survey_id' , to: "pages#move" , :as => "move_page"
  get 'survey/:id/logical_workflow' , to: "surveys#logical_workflow" , :as => "logical_workflow_survey"
  post 'survey/:id/save_logical_workflow' , to: "surveys#save_logical_workflow" , :as => "save_logical_workflow_survey"
  get 'survey/:id/layout_coloring' , to: "surveys#layout_coloring" , :as => "layout_coloring_survey"
  post 'survey/:id/save_layout_coloring' , to: "surveys#save_layout_coloring" , :as => "save_layout_coloring_survey"
  post 'next_question_logical_workflow/:id/' , to: "surveys#next_question_logical_workflow" , :as => "next_question_logical_workflow"
  get 'customer_surveys/:id' , to: "customer_surveys#survey" , :as => "customer_survey"

  resources :reports 
  get 'report/:id/:type' , to: "reports#download" , :as => "download_reports"

  root to: 'visitors#index'
end
