class CreateSurveys < ActiveRecord::Migration
  def change
    create_table :surveys do |t|
      t.string :title 
      t.string :initial_name
      t.text :description
      t.string :language
      t.timestamps null: false
    end
  end
end


