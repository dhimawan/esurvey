class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :page_title
      t.text :page_description
      t.timestamps null: false
    end
  end
end
