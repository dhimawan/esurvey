class AddNewColumnSurveyIdOnPages < ActiveRecord::Migration
  def change
    add_column :pages, :survey_id, :integer
  end
end
