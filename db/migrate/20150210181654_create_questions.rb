class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.integer :page_id
      t.string :name
      t.string :type
      t.text :question
      t.text :tooltips
      t.timestamps null: false
    end
  end
end
