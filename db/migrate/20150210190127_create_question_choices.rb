class CreateQuestionChoices < ActiveRecord::Migration
  def change
    create_table :question_choices do |t|
      t.integer :question_id
      t.string :name
      t.string :value
      t.text :description
      t.integer :order
      t.text :css_inline
      t.timestamps null: false
    end
  end
end
