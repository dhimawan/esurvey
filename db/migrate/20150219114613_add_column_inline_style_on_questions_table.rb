class AddColumnInlineStyleOnQuestionsTable < ActiveRecord::Migration
  def change
    add_column :questions, :inline_style, :text
    add_column :questions, :default_value, :text
    add_column :questions, :column_label, :text
    add_column :questions, :description_block, :text
  end
end
