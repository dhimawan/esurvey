class AddColumnCountToQuestion < ActiveRecord::Migration
  def change
    add_column :questions, :column_count, :integer
  end
end
