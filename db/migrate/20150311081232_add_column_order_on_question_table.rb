class AddColumnOrderOnQuestionTable < ActiveRecord::Migration
  def change
    add_column :questions, :order_question, :integer
  end
end
