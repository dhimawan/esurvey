class AddFinalPageToSurvey < ActiveRecord::Migration
  def change
    add_column :surveys, :final_page, :text
    add_column :surveys, :url_final_page, :string
  end
end
