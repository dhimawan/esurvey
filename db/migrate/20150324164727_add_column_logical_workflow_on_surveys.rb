class AddColumnLogicalWorkflowOnSurveys < ActiveRecord::Migration
  def change
    add_column :surveys, :is_logical_workflow, :boolean , :default => false
  end
end
