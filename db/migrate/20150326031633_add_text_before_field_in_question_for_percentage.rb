class AddTextBeforeFieldInQuestionForPercentage < ActiveRecord::Migration
  def change
    add_column :questions, :text_before_field, :string
    add_column :questions, :increment_value, :integer
    add_column :questions, :text_after_field, :string
  end
end
