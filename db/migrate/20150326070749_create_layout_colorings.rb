class CreateLayoutColorings < ActiveRecord::Migration
  def change
    create_table :layout_colorings do |t|
      t.integer :survey_id
      t.string  :font_color
      t.string  :background_color
      t.string  :row_color
      t.string  :button_font_color
      t.string  :button_background_color
      t.string  :button_border_color
      t.string  :top_border_color
      t.string  :bottom_border_color
      t.string  :right_border_color
      t.string  :left_border_color
      t.string  :width_box
      t.string  :height_box
      t.timestamps null: false
    end
  end
end



