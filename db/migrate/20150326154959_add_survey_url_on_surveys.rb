class AddSurveyUrlOnSurveys < ActiveRecord::Migration
  def change
    add_column :surveys, :survey_url, :string
  end
end
