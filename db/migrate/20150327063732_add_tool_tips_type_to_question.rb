class AddToolTipsTypeToQuestion < ActiveRecord::Migration
  def change
    add_column :questions, :as_tooltips, :boolean, default: false
  end
end
