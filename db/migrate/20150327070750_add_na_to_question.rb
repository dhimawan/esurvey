class AddNaToQuestion < ActiveRecord::Migration
  def change
    add_column :questions, :has_not_available, :boolean, default: false
    add_column :questions, :not_available_value, :string
  end
end
