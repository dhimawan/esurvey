class AddEvaluationDetailToQuestion < ActiveRecord::Migration
  def change
    add_column :questions, :open_box_field, :boolean, default: false
  end
end
