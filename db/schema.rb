# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150329171858) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "layout_colorings", force: :cascade do |t|
    t.integer  "survey_id"
    t.string   "font_color"
    t.string   "background_color"
    t.string   "row_color"
    t.string   "button_font_color"
    t.string   "button_background_color"
    t.string   "button_border_color"
    t.string   "top_border_color"
    t.string   "bottom_border_color"
    t.string   "right_border_color"
    t.string   "left_border_color"
    t.string   "width_box"
    t.string   "height_box"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "logical_workflows", force: :cascade do |t|
    t.integer  "question_id"
    t.integer  "choise_index"
    t.integer  "choise_question_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "choise_value"
  end

  create_table "pages", force: :cascade do |t|
    t.string   "page_title"
    t.text     "page_description"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "survey_id"
  end

  create_table "question_choices", force: :cascade do |t|
    t.integer  "question_id"
    t.string   "name"
    t.string   "value"
    t.text     "description"
    t.integer  "order"
    t.text     "css_inline"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "questions", force: :cascade do |t|
    t.integer  "page_id"
    t.string   "name"
    t.string   "question_type"
    t.text     "question"
    t.text     "tooltips"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.text     "choise"
    t.text     "inline_style"
    t.text     "default_value"
    t.text     "column_label"
    t.text     "description_block"
    t.integer  "column_count"
    t.integer  "order_question"
    t.string   "text_before_field"
    t.integer  "increment_value"
    t.string   "text_after_field"
    t.boolean  "as_tooltips",         default: false
    t.boolean  "has_not_available",   default: false
    t.string   "not_available_value"
    t.boolean  "open_box_field",      default: false
  end

  create_table "survey_answers", force: :cascade do |t|
    t.integer  "question_id"
    t.string   "answer"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "user_id"
    t.integer  "survey_id"
  end

  create_table "surveys", force: :cascade do |t|
    t.string   "title"
    t.string   "initial_name"
    t.text     "description"
    t.string   "language"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.boolean  "is_logical_workflow", default: false
    t.text     "final_page"
    t.string   "url_final_page"
    t.string   "survey_url"
  end

  create_table "types", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "role"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
